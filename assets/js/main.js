window.onload = function() {

   $('.phone-mask').mask('+7 (999) 9999999');

   function formattedDays(days) {
      let formattedDays;
      if (days == 1 || days == 21) {
         formattedDays = "день"
      } else {
         formattedDays = "дней"
      }

      return formattedDays;
   }

   function formatSum(sum) {
      const sumStr = sum.toString();
      let formattedSum;

      if (sum < 10000) {
      formattedSum =
         sumStr.substring(0, 1) + " " + sumStr.substring(1, sumStr.length);
      } else if (sum < 100000) {
      formattedSum =
         sumStr.substring(0, 2) + " " + sumStr.substring(2, sumStr.length);
      } else if (sum < 1000000) {
      formattedSum =
         sumStr.substring(0, 3) + " " + sumStr.substring(3, sumStr.length);
      } else if (sum >= 10000000) {
      formattedSum = 
         sumStr.substr(0, 2) + " " + sumStr.substr(2, 3) + " " + sumStr.substr(5, sumStr.length);
      } else {
      formattedSum =
         sumStr.substring(0, 1) +
         " " +
         sumStr.substring(1, 4) +
         " " +
         sumStr.substring(4, sumStr.length);
      }

      return formattedSum;
   }

   function calcSumAuto() {
      let sum = $('.calculator-slider-auto-sum').slider('value');
      let days = $('.calculator-slider-auto-time').slider('value');
  
      return formatSum(Math.ceil(sum + sum * days * 0.0833 / 100));
   }

   function calcSumPts() {
      let sum = $('.calculator-slider-pts-sum').slider('value');
      let days = $('.calculator-slider-pts-time').slider('value');
  
      return formatSum(Math.ceil((sum + sum * (days - 1) * 2.5 / 100) / days));
   }

   //calculator

   $('.calculator-tab--auto').on('click', function() {
      $('.calculator-tab--pts').removeClass('calculator-tab--active');
      $(this).addClass('calculator-tab--active');
      $('.calculator-sliders--pts').removeClass('calculator-sliders--active');
      $('.calculator-sliders--auto').addClass('calculator-sliders--active');
   });

   $('.calculator-tab--pts').on('click', function() {
      $('.calculator-tab--auto').removeClass('calculator-tab--active');
      $(this).addClass('calculator-tab--active');
      $('.calculator-sliders--auto').removeClass('calculator-sliders--active');
      $('.calculator-sliders--pts').addClass('calculator-sliders--active');
   });

   $(".calculator-slider-auto-sum").slider({
      value: 5000000,
      min: 50000,
      max: 10000000,
      step: 10000,
      orientation: "horizontal",
      range: "min",
      animate: true,
      slide: function slide(event, ui) {
        $(".format-sum-auto").text(formatSum(ui.value));
        $('.calculator-result__total').text(calcSumAuto());
      }
   });

   $(".calculator-slider-auto-time").slider({
      value: 12,
      min: 1,
      max: 30,
      step: 1,
      orientation: "horizontal",
      range: "min",
      animate: true,
      slide: function slide(event, ui) {
        $(".format-time-auto").text(ui.value);
        $(".format-time-auto--days").text(formattedDays(ui.value));
        $('.calculator-result__total').text(calcSumAuto());
      }
   });

   $(".calculator-slider-pts-sum").slider({
      value: 1500000,
      min: 100000,
      max: 3000000,
      step: 10000,
      orientation: "horizontal",
      range: "min",
      animate: true,
      slide: function slide(event, ui) {
        $(".format-sum-pts").text(formatSum(ui.value));
        $('.calculator-result__sum').text(calcSumPts());
      }
   });

   $(".calculator-slider-pts-time").slider({
      value: 18,
      min: 3,
      max: 36,
      step: 1,
      orientation: "horizontal",
      range: "min",
      animate: true,
      slide: function slide(event, ui) {
        $(".format-time-pts").text(ui.value);
        $('.calculator-result__sum').text(calcSumPts());
      }
   });


   //requirements

   $('.requirements-tab--person').on('click', function() {
      $('.requirements-tab--auto').removeClass('requirements-tab--active');
      $(this).addClass('requirements-tab--active');
      // $('.calculator-sliders--pts').removeClass('calculator-sliders--active');
      // $('.calculator-sliders--auto').addClass('calculator-sliders--active');
   });

   $('.requirements-tab--auto').on('click', function() {
      $('.requirements-tab--person').removeClass('requirements-tab--active');
      $(this).addClass('requirements-tab--active');
      // $('.calculator-sliders--auto').removeClass('calculator-sliders--active');
      // $('.calculator-sliders--pts').addClass('calculator-sliders--active');
   });

   //reviews-slider

   $('.reviews-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      appendDots: $('.reviews-slider-dots'),
      autoplay: true,
      autoplaySpeed: 4000
   })

   $('input[name="when"]').on('change', function() {
      if ($(this).val() == 'date') {
         $('.request-form-radio__date').prop('disabled', false);
      } else {
         $('.request-form-radio__date').prop('disabled', true);
      }
   })

   //tabs ur 

   $('.types-ur-tab--pts').on('click', function() {
      console.log(1);
      $('.types-ur-tab--auto').removeClass('types-ur-tab--active');
      $(this).addClass('types-ur-tab--active');
      
   });

   $('.types-ur-tab--auto').on('click', function() {
      $('.types-ur-tab--pts').removeClass('types-ur-tab--active');
      $(this).addClass('types-ur-tab--active');
   });
}